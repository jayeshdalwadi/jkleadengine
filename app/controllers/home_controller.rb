class HomeController < ApplicationController
  def index

  end
  def admin
    render "layouts/admin_application"
  end
  def get_current_user
    user = {}
    respond_to do |format|
      if user_signed_in?
        user = current_user
        format.json { render json: user ,:status => 200}
      else
        format.json { render json: user ,:status => 404}
      end
    end
  end
end
