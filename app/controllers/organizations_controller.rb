require 'csv'
class OrganizationsController < ApplicationController
  #before_filter :authenticate_user!
  before_action :set_organization, only: [:show, :edit, :update, :destroy]

  # GET /organizations
  # GET /organizations.json
  def index
    @organizations = Organization.all
    respond_to do |format|
      format.json { render json: OrganizationsDatatable.new(view_context) }
    end
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
  end

  # GET /organizations/new
  def new
    @organization = Organization.new
  end

  # GET /organizations/1/edit
  def edit
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(organization_params)

    respond_to do |format|
      if @organization.save
        format.html { redirect_to @organization, notice: 'Organization was successfully created.' }
        format.json { render action: 'show', status: :created, location: @organization }
      else
        format.html { render action: 'new' }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizations/1
  # PATCH/PUT /organizations/1.json
  def update
    respond_to do |format|
      if @organization.update(organization_params)
        format.html { redirect_to @organization, notice: 'Organization was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    @organization.destroy
    respond_to do |format|
      format.html { redirect_to organizations_url }
      format.json { head :no_content }
    end
  end
  def organization_upload
    flash[:notice] = "File uploaded Succesfully."
    organizations = []
    @update_count=0
    @create_count=0
    @count = 0
    if !params[:file].blank?
      CSV.foreach(params[:file].tempfile,:encoding => 'windows-1251:utf-8') { |row|
        row = row.first.split(",") if row.count == 1
        organization_date = {}
        organization_date["name"] = row[0]
        organization_date["email"] = row[1]
        organization_date["website"] = row[2]
        organization_date["twitter"] = row[3]
        organization_date["facebook"] = row[4]
        organization_date["phone"] = row[5]
        organization_date["remarks"] = row[6]
        organization_date["event_id"] = row[7]
        organizations << organization_date
      }
      #remove first row with column name and make a new array with activitys
      organizations = organizations[1..organizations.length]
    end
    #puts organizations.to_json
    @total_count = organizations.count
    if params[:count].blank?
      @count = 0
    else
      @count = params[:count].to_i + 1
    end

    if !organizations[@count].blank?
        @organization = Organization.new
        @organization.name=organizations[@count]["name"]
        @organization.email=organizations[@count]["email"]
        @organization.website=organizations[@count]["website"]
        @organization.twitter=organizations[@count]["twitter"]
        @organization.facebook=organizations[@count]["facebook"]
        @organization.phone=organizations[@count]["phone"]
        @organization.remarks=organizations[@count]["remarks"]
        @organization.event_id=organizations[@count]["event_id"]
        @organization.save
      end
    #respond_to do |format|
    #  format.json {render json: {data:{update_count:@update_count,create_count:@create_count,count:@count,total_count:@total_count}}}
    #end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization
      @organization = Organization.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_params
      params.require(:organization).permit(:name, :email, :website, :twitter, :facebook, :phone, :remarks, :event_id)
    end
end
