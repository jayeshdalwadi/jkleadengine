class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  respond_to :html, :json
  protect_from_forgery with: :exception

  layout :decide_layout

  private
  def decide_layout
    "#{controller_name}" == "admin" ? "admin_application" : "application"
  end
end
