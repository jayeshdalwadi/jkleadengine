json.array!(@organizations) do |organization|
  json.extract! organization, :id, :name, :email, :website, :twitter, :facebook, :phone, :remarks, :event_id
  json.url organization_url(organization, format: :json)
end
