json.array!(@events) do |event|
  json.extract! event, :id, :event_date, :values_type, :message, :status, :result, :user_id, :company_id, :campaign_id
  json.url event_url(event, format: :json)
end
