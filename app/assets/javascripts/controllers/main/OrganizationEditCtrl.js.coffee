@organization.controller 'OrganizationEditCtrl', ['$scope', '$http', '$routeParams', ($scope, $http, $routeParams) ->
  $scope.master  = {};
  $http.get("./organizations/#{$routeParams.id}.json").success((data) ->
    $scope.master = data;
    $scope.r_organization = angular.copy($scope.master)

  )


  $scope.submit = (r_organization) ->
    $scope.master = angular.copy($scope.r_organization)

    $http.put("/organizations/#{$routeParams.id}.json",organization: $scope.master)
    .success((data, status, headers, config) ->
        window.location.href  = '/#/organizations';
      ).error((data, status, headers, config) ->

    )
    return;
  $scope.reset = ->
    $scope.r_organization = angular.copy($scope.master)

#  $scope.reset();
]