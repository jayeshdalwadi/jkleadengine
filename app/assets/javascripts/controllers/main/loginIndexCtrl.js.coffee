@organization.controller 'loginIndexCtrl', ['$scope', '$http','$routeParams','AuthService', ($scope, $http, $routeParams,authservice) ->
  $scope.master ={};

  $scope.isAuthenticated = ->
    console.log(authservice.isAuth())
    return authservice.isAuth();

  $scope.getuser = ->
#    authservice.getCurrentUser()

  $scope.login = ->
    $scope.master = angular.copy($scope.user);
    console.log(authservice.isAuth());
    authservice.log_in($scope.master);

  $scope.logout = ->
    $http.delete("/users/sign_out.json",user: $scope.master,{headers: {'Content-Type': 'application/json'}})
    .then(() ->
      authToken = angular.element('meta[name="csrf-token"]').attr("content");
      $http.defaults.headers.common['X-CSRF-Token'] = authToken;
    )
    .success((data, status, headers, config) ->
#          window.location.href  = '/#/organizations';
      ).error((data, status, headers, config) ->
    )
    return;
  $scope.register = ->
    $scope.master = angular.copy($scope.user)
    $http.post("/users.json",user: $scope.master,{headers: {'Content-Type': 'application/json'}})
    .success((data, status, headers, config) ->
          window.location.href  = '/';
      ).error((data, status, headers, config) ->
    )
    return;
]
