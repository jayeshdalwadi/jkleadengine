@organization.directive "fileModel", ['$parse'
  ($parse) ->
    return (
      restrict: "A"
      link: (scope, element, attrs) ->
        model = $parse(attrs.fileModel)
        modelSetter = model.assign
        element.bind "change", ->
          scope.$apply ->
            modelSetter scope, element[0].files[0]
            return
          return
        return
    )
]

@organization.controller 'OrganizationIndexCtrl', ['$scope', '$location', '$http', ($scope, $location, $http) ->
  $scope.organizations = []
  $scope.data={}
#  $http.get('./organizations.json').success((data) ->
#    $scope.organizations = data
#  )
  $scope.ok =->
    $scope.data.count=0;
    $scope.data.total_count=1
    console.log($scope.data)
#    while ($scope.data.count != $scope.data.total_count)
    $scope.upload()
#    angular.element('#fileuploadModel').modal('hide')
    return true

  $scope.cancel = ->
    angular.element('#fileuploadModel').modal('hide')
    return true

  # Add the following lines
  $scope.viewRestaurant = (id) ->
    $location.url "/organizations/#{id}"
  $scope.upload =->
    responsedata={}
    fd = new FormData();
    fd.append('file', $scope.organizationfile);
    $http.post("/organizations/organization_upload.js",fd,{headers: {'Content-Type': undefined},transformRequest: angular.identity})
    .success((data, status, headers, config) ->
        eval(data);
      ).error((data, status, headers, config) ->
    )
    return responsedata
]