@organization.controller 'OrganizationNewCtrl', ['$scope', '$http', '$routeParams', ($scope, $http, $routeParams) ->
#http://jsfiddle.net/JeJenny/ZG9re/
  $scope.master  = {};

  $scope.submit = (r_organization) ->
    $scope.master = angular.copy($scope.r_organization)

    $http.post("/organizations.json",organization: $scope.master)
    .success((data, status, headers, config) ->
        window.location.href  = '/#/organizations';
     ).error((data, status, headers, config) ->

    )
    return;
  $scope.reset = ->
    $scope.r_organization = angular.copy($scope.master)

  $scope.reset();
]