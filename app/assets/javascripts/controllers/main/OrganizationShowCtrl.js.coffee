@organization.controller 'OrganizationShowCtrl', ['$scope', '$http', '$routeParams', ($scope, $http, $routeParams) ->
  $http.get("./organizations/#{$routeParams.id}.json").success((data) ->
    $scope.organization = data
  )
]