# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#= require_self
#= require_tree ./services/global
#= require_tree ./services/main
#= require_tree ./filters/global
#= require_tree ./filters/main
#= require_tree ./controllers/global
#= require_tree ./controllers/main
#= require_tree ./directives/global
#= require_tree ./directives/main

@organization = angular.module('organization', ['ngRoute'])

@organization.config(['$httpProvider',($httpProvider) ->
  authToken = angular.element('meta[name="csrf-token"]').attr("content");
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken;
]);

@organization.factory "UserSession", ->
  currUserId = null
  createSession: (userId) ->
    currUserId = userId
    return

  destroySession: ->
    currUserId = null
    return

  exists: ->
    currUserId isnt null

@organization.service "AuthService", ($http, $q) ->
  currentUser = {}
  getCurrentUser = ->
    $http.get("/home/get_current_user.json",{headers: {'Content-Type': 'application/json'}}
    ).success((data, status, headers, config) ->
      updateCurrentUser(data,true)
    ).error((data, status, headers, config) ->
      updateCurrentUser({},false)
    )
    currentUser

  log_in = (data)->
    $http.post("/users/sign_in.json",user:data,{headers: {'Content-Type': 'application/json'}})
    .success((data, status, headers, config) ->
        updateCurrentUser(data.user,true)
        window.location.href  = '/';
    ).error((data, status, headers, config) ->
        updateCurrentUser({},false)
    )

  updateCurrentUser = (user, authorized)->
    if user
      currentUser.user = user
    currentUser.authorized = authorized

  logout = (data)->
    console.log("logout");

  isAuth = ->
    if (currentUser.authorized == false || currentUser.authorized ==  undefined)
      false
    else
      true

  return (
    log_in: log_in
    logout: logout
    getCurrentUser: getCurrentUser
    isAuth: isAuth
    updateCurrentUser: updateCurrentUser
  )
  return

@organization.directive "checkUser", [ "$rootScope","$location","AuthService",($root, $location, userSrv) ->
  return link: (scope, elem, attrs, ctrl) ->
    $root.$on "$routeChangeStart", (event, currRoute, prevRoute) ->
      console.log(userSrv.isAuthenticated());
      console.log(userSrv.getCurrentUser());
      if not prevRoute.access.isFree and not userSrv.isLogged

        return
      return
]

# http://blog.brunoscopelliti.com/deal-with-users-authentication-in-an-angularjs-web-app

@organization.config(['$routeProvider', ($routeProvider) ->
  $routeProvider.when('/organizations', {
    templateUrl: '../templates/organization/index.html',
    access: {isFree: false},
    controller: 'OrganizationIndexCtrl'
  }).when('/organizations/:id', {
    templateUrl: '../templates/organization/show.html',
    access: {isFree: false},
    controller: 'OrganizationShowCtrl'
  }).when('/organization/new', {
    templateUrl: '../templates/organization/new.html',
    access: {isFree: true},
    controller: 'OrganizationNewCtrl'
  }).when('/organizations/:id/edit', {
    templateUrl: '../templates/organization/edit.html',
    controller: 'OrganizationEditCtrl'
  }).when('/admin', {
    templateUrl: '../templates/admin/index.html',
    controller: 'AdminIndexCtrl'
  }).when('/login', {
    templateUrl: '../templates/users/login.html',
    controller: 'loginIndexCtrl'
  }).when('/sign_up', {
      templateUrl: '../templates/users/register.html',
      controller: 'loginIndexCtrl'
    })
  .otherwise({
    templateUrl: '../templates/home.html',
    controller: 'OrganizationCtrl'
  })
])
.run( [ "$rootScope","$location","AuthService",($root, $location, userSrv) ->
  $root.$on "$routeChangeStart", (event, currRoute, prevRoute) ->
    console.log(userSrv.isAuth());
  if(userSrv.isAuth() == undefined)
    userSrv.getCurrentUser()
  if (userSrv.isAuth() == false || userSrv.isAuth() == undefined)
    $location.path('/login');
  else
    $location.path('/');
])

