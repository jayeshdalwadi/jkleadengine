class Organization < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  settings index: { number_of_shards: 1 } do
    mappings dynamic: 'false' do
      indexes :name, analyzer: 'text', index_options: 'offsets'
      indexes :email, analyzer: 'text'
    end
  end
  def self.search(query)
    __elasticsearch__.search(
        {
            query: {
                #query_string: {
                #   fields:  ['name^10', 'email','phone','website'],
                #   query: query
                #  }
                multi_match: {
                    query: "*#{query}*",
                    fields: ['name^10', 'email','phone','website']
                }
            }
        }
    )
  end
end
Organization.import