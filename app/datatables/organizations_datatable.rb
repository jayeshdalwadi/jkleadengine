class OrganizationsDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Organization.count,
        iTotalDisplayRecords: organizations.map.count,
        iDisplayLength:organizations.map.count,
        aaData: data
    }
  end

  private

  def data
    organizations.map do |organization|
      [
          link_to(organization.name,"/#/organizations/#{organization.id}"),
          organization.email,
          organization.website,
          organization.facebook,
          organization.phone,
          organization.remarks

      ]
    end
  end

  def organizations
    @organizations ||= fetch_organizations
  end

  def fetch_organizations
    organizations = Organization.order("#{sort_column} #{sort_direction}")
    organizations = organizations.page(page).per_page(per_page)
    if params[:search].present? && params[:search][:value].present? && params[:search][:value].length > 1
      #organizations = organizations.where("name like :search or email like :search", search: "%#{params[:search][:value]}%")
      organizations = organizations.search params[:search][:value]
    end
    organizations
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

  def sort_column
    columns = %w[name email]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end