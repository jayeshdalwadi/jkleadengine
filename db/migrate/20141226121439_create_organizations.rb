class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :email
      t.string :website
      t.string :twitter
      t.string :facebook
      t.string :phone
      t.text :remarks
      t.integer :event_id

      t.timestamps
    end
  end
end
