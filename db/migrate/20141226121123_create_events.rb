class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :event_date
      t.string :values_type
      t.text :message
      t.boolean :status
      t.string :result
      t.integer :user_id
      t.integer :company_id
      t.integer :campaign_id

      t.timestamps
    end
  end
end
